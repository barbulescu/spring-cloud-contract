package ch.barbulescu.contract.consumer

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import java.time.Duration

@SpringBootApplication
class ConsumerApplication

fun main(vararg args: String) {
    runApplication<ConsumerApplication>(*args)
}

class HelloRunner(private val helloClient: HelloClient) : ApplicationRunner {

    override fun run(args: ApplicationArguments) {
        println(helloClient.sayHello("Marius", "DE"))
    }
}

private const val TIMEOUT = 5L

@Component
class HelloClient(private val webClientBuilder: WebClient.Builder) {

    fun sayHello(name: String, language: String): String? {
        val webClient = webClientBuilder
            .baseUrl("http://localhost:8081")
            .build()

        return webClient.get()
            .uri {
                it.path("/hello/$name")
                    .queryParam("language", language)
                    .build()
            }
            .retrieve()
            .bodyToMono(String::class.java)
            .block(Duration.ofSeconds(TIMEOUT))
    }
}
