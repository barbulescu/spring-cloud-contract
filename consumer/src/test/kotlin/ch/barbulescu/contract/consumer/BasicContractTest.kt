package ch.barbulescu.contract.consumer

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode.LOCAL
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.test.context.TestConstructor
import org.springframework.test.context.TestConstructor.AutowireMode.ALL

@SpringBootTest(webEnvironment = NONE)
@AutoConfigureStubRunner(
    stubsMode = LOCAL,
    ids = ["ch.barbulescu.contract:provider:+:stubs:8081"],
    failOnNoStubs = false
)
@TestConstructor(autowireMode = ALL)
@AutoConfigureWireMock
class BasicContractTest(private val helloClient: HelloClient) {

    @Test
    fun `simple contract testing`() {
        val response = helloClient.sayHello("Marius", "EN")
        assertThat(response).isEqualTo("Hello Marius!")
    }
}