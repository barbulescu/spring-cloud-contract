package ch.barbulescu.contract.consumer

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ConsumerApplicationTests {

	@Test
	fun contextLoads() {
		//tests Spring context is starting correctly
	}

}
