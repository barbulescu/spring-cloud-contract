package ch.barbulescu.contract.provider

import org.springframework.stereotype.Component

@Component
class HelloMessageProvider {
    fun helloMessage(name: String, language: String) = when (language) {
        "en", "EN" -> "Hello $name!"
        "de", "DE" -> "Hallo $name!"
        else -> error("Language $language is not supported")
    }
}