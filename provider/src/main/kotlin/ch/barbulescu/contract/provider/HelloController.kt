package ch.barbulescu.contract.provider

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloController(val helloMessageProvider: HelloMessageProvider) {

    @GetMapping("/hello/{name}")
   suspend fun sayHello(@PathVariable name: String, @RequestParam language: String): String {
        val helloMessage = helloMessageProvider.helloMessage(name, language)
        println("hello $name + $language -> $helloMessage")
        return helloMessage
    }
}
