package ch.barbulescu.contract.provider

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ProviderApplication

fun main(vararg args: String) {
    runApplication<ProviderApplication>(*args)
}
