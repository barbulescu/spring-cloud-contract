import org.springframework.cloud.contract.spec.ContractDsl.Companion.contract

contract {
    name = "hello"
    request {
        method = GET
        url = url("/hello/Marius?language=EN")
    }
    response {
        status = OK
        body = body("Hello ${fromRequest().path(1)}!")
        headers {
            contentType = TEXT_PLAIN
        }
    }
}