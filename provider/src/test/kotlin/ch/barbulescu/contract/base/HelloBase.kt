package ch.barbulescu.contract.base

import ch.barbulescu.contract.provider.HelloController
import ch.barbulescu.contract.provider.HelloMessageProvider
import io.restassured.module.webtestclient.RestAssuredWebTestClient
import org.junit.jupiter.api.BeforeEach


open class HelloBase {
    @BeforeEach
    fun before() {
        val helloMessageProvider = HelloMessageProvider()
        val helloController = HelloController(helloMessageProvider)
        RestAssuredWebTestClient.standaloneSetup(helloController)
    }
}
